plugins {
    id("com.github.johnrengelman.shadow") version "7.1.2"
    id("io.micronaut.application") version "3.6.0"
}

version = "0.1"
group = "com.gitlab.taucher2003"

repositories {
    mavenCentral()
    maven(url = "https://gitlab.com/api/v4/groups/12234336/-/packages/maven")
}

dependencies {
    annotationProcessor("io.micronaut:micronaut-http-validation")
    annotationProcessor("io.micronaut:micronaut-inject-java")
    implementation("io.micronaut:micronaut-http-client")
    implementation("io.micronaut:micronaut-jackson-databind")
    implementation("jakarta.annotation:jakarta.annotation-api")
    runtimeOnly("ch.qos.logback:logback-classic")
    implementation("io.micronaut:micronaut-validation")
    implementation("com.gitlab.taucher2003.t2003-utils:log:1.1-beta.13")
    implementation("com.gitlab.taucher2003.t2003-utils:tjda:1.1-beta.13")
}

application {
    mainClass.set("com.gitlab.taucher2003.discord_gitlab_chatops.Application")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

micronaut {
    runtime("netty")
    testRuntime("junit5")
    processing {
        incremental(true)
        annotations("com.gitlab.taucher2003.*")
    }
}
