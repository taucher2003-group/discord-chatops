package com.gitlab.taucher2003.discord_gitlab_chatops;

import io.micronaut.context.annotation.Value;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.scheduling.annotation.Scheduled;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.channel.middleman.MessageChannel;
import net.dv8tion.jda.api.interactions.commands.CommandInteraction;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Client("${gitlab.url}")
public abstract class ChatopsExecutor {

    private final Map<String, Pair> interactionMap = new HashMap<>();

    public record Pair(CommandInteraction interaction, Instant created) {}

    private final String endpoint;

    public ChatopsExecutor(
            @Value("${incoming.host}") String host,
            @Value("${incoming.path}") String path
    ) {
        this.endpoint = host + path;
    }

    @Post(
            value = "/api/v4/projects/${gitlab.project}/integrations/mattermost_slash_commands/trigger?token=${gitlab.token}",
            processes = MediaType.APPLICATION_JSON,
            produces = MediaType.APPLICATION_JSON
    )
    public abstract Response executeChatops(@Body Payload body);

    public static String unslack(String input) {
        return input.replaceAll("<([^|]+)\\|(.+?)>", "[$2]($1)");
    }

    public Payload buildPayload(CommandInteraction event, String input) {
        var id = "%s%s%s".formatted(event.getId(), event.getUser().getId(), event.getChannel().getId());
        var url = "%s/%s".formatted(endpoint, id);

        interactionMap.put(id, new Pair(event, Instant.now()));

        return new ChatopsExecutor.Payload(
                event.getGuild().getId(),
                event.getUser().getId(),
                event.getGuild().getName(),
                event.getUser().getAsTag(),
                input,
                event.getChannel().getId(),
                url
        );
    }

    public void sendForToken(String token, MessageEmbed embed) {
        if(!interactionMap.containsKey(token)) {
            return;
        }

        var interaction = interactionMap.get(token).interaction();

        if(interaction.getHook().isExpired()) {
            ((MessageChannel)interaction.getChannel()).sendMessageEmbeds(embed).queue();
        } else {
            interaction.getHook().sendMessageEmbeds(embed).queue();
        }
    }

    @Scheduled(fixedRate = "1h")
    public void cleanup() {
        interactionMap.entrySet().removeIf(entry -> entry.getValue().created().isBefore(Instant.now().minus(12, ChronoUnit.HOURS)));
    }

    public static MessageEmbed fromMattermost(ChatopsExecutor.EmbedResponse response) {
        var builder = new EmbedBuilder();
        if (response.title() != null) {
            builder.setTitle(response.title(), response.title_link());
        }
        if(response.author_name() != null) {
            builder.setAuthor(response.author_name(), null, response.author_icon());
        }
        if(response.text() != null) {
            builder.setDescription(unslack(response.text()));
        }
        if(response.color() != null) {
            builder.setColor(Integer.decode(response.color().replace("#", "0x")));
        }
        if(response.fields() != null) {
            response.fields().stream()
                    .limit(Message.MAX_EMBED_COUNT)
                    .map(field -> new MessageEmbed.Field(field.title(), field.value(), false))
                    .forEachOrdered(builder::addField);
        }
        return builder.build();
    }

    public record Payload(
            String team_id,
            String user_id,
            String team_domain,
            String user_name,
            String text,
            String channel_id,
            String response_url
    ) {}

    public record Response(
            String text,
            String response_type,
            String message,
            List<EmbedResponse> attachments
    ) {}

    public record EmbedResponse(
            String title,
            String title_link,
            String author_name,
            String author_icon,
            String fallback,
            String pretext,
            String text,
            String color,
            List<EmbedFieldResponse> fields
    ) {}

    public record EmbedFieldResponse(
            String title,
            String value
    ) {}
}
