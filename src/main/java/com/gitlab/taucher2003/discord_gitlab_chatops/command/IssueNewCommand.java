package com.gitlab.taucher2003.discord_gitlab_chatops.command;

import com.gitlab.taucher2003.discord_gitlab_chatops.ChatopsExecutor;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.CommandArgument;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.Permissible;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.SubCommand;
import com.gitlab.taucher2003.t2003_utils.tjda.theme.Theme;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import jakarta.inject.Singleton;
import net.dv8tion.jda.api.interactions.commands.CommandInteraction;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;

@Singleton
@Context
@GroupedSubCommand("issue")
@Requires(property = "command.enabled.issue.new", value = "true")
public class IssueNewCommand extends SubCommand {
    private final ChatopsExecutor executor;

    public IssueNewCommand(ChatopsExecutor executor) {
        super(createMeta("new", "Create a new issue")
                .addArgument(CommandArgument.text("title", "The title of the issue").setRequired(true).build())
                .addArgument(CommandArgument.text("description", "The description of the issue").build())
                .build());
        this.executor = executor;
    }

    @Override
    public void execute(CommandInteraction event, Theme theme, Permissible.PermissibleContext permissibleContext) {
        event.deferReply().queue(hook -> {
            var response = executor.executeChatops(
                    executor.buildPayload(event, "issue new %s\n%s".formatted(
                            findOption(event, "title").getAsString(),
                            findOptionOpt(event, "description").map(OptionMapping::getAsString).orElse("")
                    ))
            );

            if(response.attachments() != null && !response.attachments().isEmpty()) {
                hook.sendMessageEmbeds(ChatopsExecutor.fromMattermost(response.attachments().get(0))).queue();
                return;
            }

            hook.sendMessage(ChatopsExecutor.unslack(response.text())).queue();
        });
    }
}
