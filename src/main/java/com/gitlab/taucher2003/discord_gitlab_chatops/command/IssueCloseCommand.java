package com.gitlab.taucher2003.discord_gitlab_chatops.command;

import com.gitlab.taucher2003.discord_gitlab_chatops.ChatopsExecutor;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.CommandArgument;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.Permissible;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.SubCommand;
import com.gitlab.taucher2003.t2003_utils.tjda.theme.Theme;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import jakarta.inject.Singleton;
import net.dv8tion.jda.api.interactions.commands.CommandInteraction;

@Singleton
@Context
@GroupedSubCommand("issue")
@Requires(property = "command.enabled.issue.close", value = "true")
public class IssueCloseCommand extends SubCommand {

    private final ChatopsExecutor executor;

    public IssueCloseCommand(ChatopsExecutor executor) {
        super(createMeta("close", "Close an issue")
                .addArgument(CommandArgument.integer("id", "Issue ID").setRequired(true).build())
                .build());
        this.executor = executor;
    }

    @Override
    public void execute(CommandInteraction event, Theme theme, Permissible.PermissibleContext permissibleContext) {
        event.deferReply().queue(hook -> {
            var response = executor.executeChatops(
                    executor.buildPayload(event, "issue close %s".formatted(findOption(event, "id").getAsLong()))
            );

            if(response.attachments() != null && !response.attachments().isEmpty()) {
                hook.sendMessageEmbeds(ChatopsExecutor.fromMattermost(response.attachments().get(0))).queue();
                return;
            }

            hook.sendMessage(ChatopsExecutor.unslack(response.text())).queue();
        });
    }
}
