package com.gitlab.taucher2003.discord_gitlab_chatops.command;

import com.gitlab.taucher2003.discord_gitlab_chatops.ChatopsExecutor;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.Permissible;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.SubCommand;
import com.gitlab.taucher2003.t2003_utils.tjda.theme.Theme;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import jakarta.inject.Singleton;
import net.dv8tion.jda.api.interactions.commands.CommandInteraction;

@Singleton
@Context
@TopLevelSubCommand
@Requires(property = "command.enabled.help", value = "true")
public class HelpCommand extends SubCommand {
    private final ChatopsExecutor executor;

    public HelpCommand(ChatopsExecutor executor) {
        super(createMeta("help", "View the help").build());
        this.executor = executor;
    }

    @Override
    public void execute(CommandInteraction event, Theme theme, Permissible.PermissibleContext permissibleContext) {
        event.deferReply(true).queue(hook -> {
            hook.setEphemeral(true);

            var response = executor.executeChatops(executor.buildPayload(event, "help"));

            hook.editOriginal(ChatopsExecutor.unslack(response.text())).queue();
        });
    }
}
