package com.gitlab.taucher2003.discord_gitlab_chatops.command;

import com.gitlab.taucher2003.discord_gitlab_chatops.ChatopsExecutor;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.CommandArgument;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.Permissible;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.SubCommand;
import com.gitlab.taucher2003.t2003_utils.tjda.theme.Theme;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import jakarta.inject.Singleton;
import net.dv8tion.jda.api.interactions.commands.CommandInteraction;

@Singleton
@Context
@TopLevelSubCommand
@Requires(property = "command.enabled.deploy", value = "true")
public class DeployCommand extends SubCommand {
    private final ChatopsExecutor executor;

    public DeployCommand(ChatopsExecutor executor) {
        super(createMeta("deploy", "Deploy from an environment to another environment")
                        .addArgument(CommandArgument.text("from", "The source environment").setRequired(true).build())
                        .addArgument(CommandArgument.text("to", "The target environment").setRequired(true).build())
                        .build());
        this.executor = executor;
    }

    @Override
    public void execute(CommandInteraction event, Theme theme, Permissible.PermissibleContext permissibleContext) {
        event.deferReply().queue(hook -> {
            var response = executor.executeChatops(executor.buildPayload(event, "deploy %s to %s".formatted(
                    findOption(event, "from").getAsString(),
                    findOption(event, "to").getAsString()
            ).trim()));

            hook.editOriginal(ChatopsExecutor.unslack(response.text())).queue();
        });
    }
}
