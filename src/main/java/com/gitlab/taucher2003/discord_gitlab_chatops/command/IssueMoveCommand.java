package com.gitlab.taucher2003.discord_gitlab_chatops.command;

import com.gitlab.taucher2003.discord_gitlab_chatops.ChatopsExecutor;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.CommandArgument;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.Permissible;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.SubCommand;
import com.gitlab.taucher2003.t2003_utils.tjda.theme.Theme;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import jakarta.inject.Singleton;
import net.dv8tion.jda.api.interactions.commands.CommandInteraction;

@Singleton
@Context
@GroupedSubCommand("issue")
@Requires(property = "command.enabled.issue.move", value = "true")
public class IssueMoveCommand extends SubCommand {

    private final ChatopsExecutor executor;

    public IssueMoveCommand(ChatopsExecutor executor) {
        super(createMeta("move", "Move an issue to a different project")
                .addArgument(CommandArgument.integer("id", "Issue ID").setRequired(true).build())
                .addArgument(CommandArgument.text("project", "The project path of the new project").setRequired(true).build())
                .build());
        this.executor = executor;
    }

    @Override
    public void execute(CommandInteraction event, Theme theme, Permissible.PermissibleContext permissibleContext) {
        event.deferReply().queue(hook -> {
            var response = executor.executeChatops(
                    executor.buildPayload(event, "issue move %s to %s".formatted(
                            findOption(event, "id").getAsLong(),
                            findOption(event, "project").getAsString()
                    ))
            );

            if(response.attachments() != null && !response.attachments().isEmpty()) {
                hook.sendMessageEmbeds(ChatopsExecutor.fromMattermost(response.attachments().get(0))).queue();
                return;
            }

            hook.sendMessage(ChatopsExecutor.unslack(response.text())).queue();
        });
    }
}
