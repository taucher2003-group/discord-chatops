package com.gitlab.taucher2003.discord_gitlab_chatops.command;

import com.gitlab.taucher2003.discord_gitlab_chatops.ChatopsExecutor;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.CommandArgument;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.Permissible;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.SubCommand;
import com.gitlab.taucher2003.t2003_utils.tjda.theme.Theme;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import jakarta.inject.Singleton;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.interactions.commands.CommandInteraction;
import net.dv8tion.jda.api.utils.messages.MessageCreateBuilder;

import java.util.stream.Collectors;

@Singleton
@Context
@GroupedSubCommand("issue")
@Requires(property = "command.enabled.issue.search", value = "true")
public class IssueSearchCommand extends SubCommand {
    private final ChatopsExecutor executor;

    public IssueSearchCommand(ChatopsExecutor executor) {
        super(createMeta("search", "Search for issues")
                        .addArgument(CommandArgument.text("query", "Search Query").setRequired(true).build())
                        .build());
        this.executor = executor;
    }

    @Override
    public void execute(CommandInteraction event, Theme theme, Permissible.PermissibleContext permissibleContext) {
        event.deferReply(true).queue(hook -> {
            var response = executor.executeChatops(
                    executor.buildPayload(event, "issue search %s".formatted(findOption(event, "query").getAsString()))
            );

            var messageCreateData = new MessageCreateBuilder()
                    .setContent(response.text())
                    .addEmbeds(response.attachments().stream().limit(Message.MAX_EMBED_COUNT).map(ChatopsExecutor::fromMattermost).collect(Collectors.toList()))
                    .build();

            hook.sendMessage(messageCreateData).queue();
        });
    }
}
