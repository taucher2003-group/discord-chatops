package com.gitlab.taucher2003.discord_gitlab_chatops.command;

import com.gitlab.taucher2003.discord_gitlab_chatops.ChatopsExecutor;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.CommandArgument;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.Permissible;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.SubCommand;
import com.gitlab.taucher2003.t2003_utils.tjda.theme.Theme;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import jakarta.inject.Singleton;
import net.dv8tion.jda.api.interactions.commands.CommandInteraction;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;

@Singleton
@Context
@TopLevelSubCommand
@Requires(property = "command.enabled.run", value = "true")
public class RunCommand extends SubCommand {
    private final ChatopsExecutor executor;

    public RunCommand(ChatopsExecutor executor) {
        super(createMeta("run", "Run CI Jobs")
                        .addArgument(CommandArgument.text("job", "The target job").setRequired(true).build())
                        .addArgument(CommandArgument.text("arguments", "Additional arguments").build())
                        .build());
        this.executor = executor;
    }

    @Override
    public void execute(CommandInteraction event, Theme theme, Permissible.PermissibleContext permissibleContext) {
        event.deferReply().queue(hook -> {
            var response = executor.executeChatops(executor.buildPayload(event, "run %s %s".formatted(
                    findOption(event, "job").getAsString(),
                    findOptionOpt(event, "arguments").map(OptionMapping::getAsString).orElse("")
            ).trim()));

            hook.editOriginal(ChatopsExecutor.unslack(response.text())).queue();
        });
    }
}
