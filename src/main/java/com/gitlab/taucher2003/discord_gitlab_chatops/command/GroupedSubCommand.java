package com.gitlab.taucher2003.discord_gitlab_chatops.command;

import jakarta.inject.Qualifier;

@Qualifier
public @interface GroupedSubCommand {

    String value();
}
