package com.gitlab.taucher2003.discord_gitlab_chatops.command;

import com.gitlab.taucher2003.t2003_utils.tjda.commands.CommandGroup;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.SubCommand;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Requires;
import jakarta.inject.Singleton;

import java.util.Arrays;

@Singleton
@Context
@Requires(property = "command.enabled.issue", notEquals = "false")
public class IssueCommandGroup extends CommandGroup {
    public IssueCommandGroup(@GroupedSubCommand("issue") SubCommand[] issueCommands) {
        super(createMeta("issue", "Manage issues")
                        .setSubCommands(Arrays.asList(issueCommands))
                        .build());
    }
}
