package com.gitlab.taucher2003.discord_gitlab_chatops;

import com.gitlab.taucher2003.t2003_utils.tjda.commands.Command;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.SlashCommandManager;
import com.gitlab.taucher2003.t2003_utils.tjda.connect.ShardManagerConnector;
import com.gitlab.taucher2003.t2003_utils.tjda.theme.MonokaiTheme;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Factory;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.annotation.Value;
import jakarta.inject.Singleton;
import net.dv8tion.jda.api.sharding.DefaultShardManagerBuilder;
import net.dv8tion.jda.api.sharding.ShardManager;

import java.util.List;

@Factory
public class DiscordBot {

    @Bean
    @Context
    @Singleton
    @Requires(notEnv = "test")
    public ShardManager shardManager(
            @Value("${discord.token}") String token,
            @Value("${discord.guild}") long guildId,
            @Value("${discord.upsert-commands:true}") boolean upsertCommands,
            SlashCommandManager manager
    ) throws InterruptedException {
        var builder = DefaultShardManagerBuilder.createLight(token);
        var shardManager = new ShardManagerConnector(builder)
                .withSlashCommandManager(manager)
                .build();

        for(var shard : shardManager.getShards()) {
            shard.awaitReady();
        }

        if(upsertCommands) {
            manager.upsertCommands(shardManager.getGuildById(guildId));
        } else {
            manager.updateCommands(shardManager.getGuildById(guildId));
        }

        return shardManager;
    }

    @Bean
    @Singleton
    public SlashCommandManager slashCommandManager(List<Command> commands) {
        var manager = new SlashCommandManager(new MonokaiTheme());
        commands.forEach(manager::registerCommand);
        return manager;
    }
}
