package com.gitlab.taucher2003.discord_gitlab_chatops;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.annotation.Post;

@Controller("${incoming.path}/{token}")
public class IncomingController {

    private final ChatopsExecutor executor;

    public IncomingController(ChatopsExecutor executor) {
        this.executor = executor;
    }

    @Post
    public HttpResponse<?> post(@PathVariable("token") String token, @Body ChatopsExecutor.Response body) {
        var embed = ChatopsExecutor.fromMattermost(body.attachments().get(0));

        executor.sendForToken(token, embed);

        return HttpResponse.ok();
    }
}
