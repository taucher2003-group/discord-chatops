package com.gitlab.taucher2003.discord_gitlab_chatops;

import com.gitlab.taucher2003.discord_gitlab_chatops.command.TopLevelSubCommand;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.Command;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.CommandGroup;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.Permissible;
import com.gitlab.taucher2003.t2003_utils.tjda.commands.SubCommand;
import com.gitlab.taucher2003.t2003_utils.tjda.theme.Theme;
import io.micronaut.context.annotation.Context;
import io.micronaut.context.annotation.Value;
import jakarta.inject.Singleton;
import net.dv8tion.jda.api.interactions.commands.CommandInteraction;

import java.util.Arrays;

@Singleton
@Context
public class ChatopsCommand extends Command {
    public ChatopsCommand(
            @TopLevelSubCommand SubCommand[] subcommands,
            CommandGroup[] commandGroups,
            @Value("${discord.command:chatops}") String commandName
    ) {
        super(createMeta(commandName, "Run GitLab ChatOps from Discord")
                        .setSubCommands(Arrays.asList(subcommands))
                        .setGroups(Arrays.asList(commandGroups))
                        .build());
    }

    @Override
    public void execute(CommandInteraction event, Theme theme, Permissible.PermissibleContext permissibleContext) {

    }
}



